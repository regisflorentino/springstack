package com.rfsystems.estudos.spring.batch.model;

import lombok.Data;

@Data
public class EmpresaDTO {

    private String chave;
    private String codigo;
}
