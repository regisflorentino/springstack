package com.rfsystems.estudos.spring.batch.processor;

import com.rfsystems.estudos.spring.batch.model.EmpresaDTO;
import com.rfsystems.estudos.spring.batch.model.EmpresaEntity;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;


@Component
public class DocProcessor implements ItemProcessor<EmpresaDTO, EmpresaEntity> {

    //aqui eu crio uma validação para que em cada item do arquivo seja verificado se ele obedece as restrições de chave
    //eu poderia fazer essa validação nno momento da persistencia, mas o hibernate daria rollback no job no caso de falha em um item
    @Override
    public EmpresaEntity process(EmpresaDTO empDTO) throws Exception {

        EmpresaEntity empresa = new EmpresaEntity();
        empresa.setCodigo(empDTO.getCodigo());
        empresa.setChave(empDTO.getChave());

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();

        Set<ConstraintViolation<EmpresaEntity>> constraintViolations = validator.validate(empresa);
        if (constraintViolations.size() > 0){
            System.out.println("Registro com chave inválida " + empresa.toString());
            empresa = new EmpresaEntity();
        }
        return empresa;
    }

}
