package com.rfsystems.estudos.spring.batch.model;


import lombok.Data;
import org.springframework.boot.context.properties.bind.DefaultValue;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "empresa")
public class EmpresaEntity {

    @Id
    @Size(max = 44)
    @Digits(integer = 44, fraction = 0)
    private String chave;

    private String codigo;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

}
