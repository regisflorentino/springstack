package com.rfsystems.estudos.spring.batch.writer;

import com.rfsystems.estudos.spring.batch.model.EmpresaEntity;
import com.rfsystems.estudos.spring.batch.repository.EmpresaRepo;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmpresaDBWriter implements ItemWriter<EmpresaEntity> {

    @Autowired
    private EmpresaRepo employeeRepo;

    @Override
    public void write(List<? extends EmpresaEntity> empresa)  {
        try {
            employeeRepo.saveAll(empresa);
        } catch (Exception e) {
            System.out.println("Error ao tentar persistir registros no banco " + empresa);
        }

    }
}
