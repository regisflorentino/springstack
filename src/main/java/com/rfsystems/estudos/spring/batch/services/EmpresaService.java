package com.rfsystems.estudos.spring.batch.services;


import com.rfsystems.estudos.spring.batch.model.EmpresaEntity;
import com.rfsystems.estudos.spring.batch.model.StatusEnum;
import com.rfsystems.estudos.spring.batch.repository.EmpresaRepo;
import com.rfsystems.estudos.spring.batch.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class EmpresaService {

    @Autowired
    private EmpresaRepo repo;

    public Response updateStatus(List<String> keys){
        Response response = new Response();
        keys.forEach( key -> {
            if(hasKeyValidLength(key)  &&  hasJustNumbers(key)){
                Optional<EmpresaEntity> empresaEntity = repo.findByChave(key);
                if(empresaEntity.isPresent()){
                    empresaEntity.get().setStatus(StatusEnum.VALIDADO);
                    repo.save(empresaEntity.get());
                    response.getValidKeys().add(key);
                }else {
                    response.getInvalidKeys().add(key);
                }
            } else {
                response.getInvalidKeys().add(key);
            }
        });
        return response;

    }

    public Page<EmpresaEntity> getByCodido(Pageable pageable, String codigo){
        return repo.findAllByCodigo(codigo, pageable);
    }

    private boolean hasJustNumbers(String key) {
        return key.matches("[0-9]+");
    }

    private boolean hasKeyValidLength(String key) {
        return  key.length() == 44;
    }


}
