package com.rfsystems.estudos.spring.batch.controller;

import com.rfsystems.estudos.spring.batch.model.EmpresaEntity;
import com.rfsystems.estudos.spring.batch.response.Response;
import com.rfsystems.estudos.spring.batch.services.EmpresaService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/run")
@Api(value = "API avaliação DOOTAX")
public class JobController {

    @Autowired
    private EmpresaService service;

    @PostMapping(path = "/updateStatus", consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Response> addMember(@RequestBody List<String> chaves) {

        Response response = service.updateStatus(chaves);
        return ResponseEntity.status(HttpStatus.OK).body(response);

    }

    @GetMapping(path = "/empresas",  produces = "application/json")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<Page<EmpresaEntity>> addMember(@RequestParam(value = "codigo") String codigo,
                                                         @RequestParam(defaultValue = "0") Integer pageNo,
                                                         @RequestParam(defaultValue = "20") Integer pageSize) {
        pageSize = limitPageSize(pageSize);
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by("codigo"));
        Page<EmpresaEntity> empresaEntities = service.getByCodido(paging, codigo);
        if(empresaEntities.getTotalElements() > 0){
            return ResponseEntity.status(HttpStatus.OK).body(empresaEntities);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(empresaEntities);
        }
    }

    private int limitPageSize(Integer size){
        if(size > 20){ return 20; }
        return size;
    }

}
