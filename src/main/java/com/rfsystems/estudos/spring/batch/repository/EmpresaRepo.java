package com.rfsystems.estudos.spring.batch.repository;

import com.rfsystems.estudos.spring.batch.model.EmpresaEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmpresaRepo extends PagingAndSortingRepository<EmpresaEntity, String> {

    public Optional<EmpresaEntity> findByChave(String chave);

    public Page<EmpresaEntity> findAllByCodigo(String codigo, Pageable pageable);

}
