package com.rfsystems.estudos.spring.batch.mapper;

import com.rfsystems.estudos.spring.batch.model.EmpresaDTO;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

public class EmpresaFileRowMapper implements FieldSetMapper<EmpresaDTO> {

    @Override
    public EmpresaDTO mapFieldSet(FieldSet fieldSet) {
        EmpresaDTO empresa = new EmpresaDTO();
        empresa.setChave(fieldSet.readString("chave"));
        empresa.setCodigo(fieldSet.readString("codigo"));
        return empresa;
    }

}
