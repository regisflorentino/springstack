package com.rfsystems.estudos.spring.batch.model;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.time.DateTimeException;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "Log")
public class LogEntity {

    @Id
    private String id;

    private LocalDate data;

    private String ip;

    private String request;

    private String userAgente;

}
