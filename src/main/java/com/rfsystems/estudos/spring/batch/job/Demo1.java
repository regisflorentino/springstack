package com.rfsystems.estudos.spring.batch.job;


import com.rfsystems.estudos.spring.batch.mapper.EmpresaFileRowMapper;
import com.rfsystems.estudos.spring.batch.model.EmpresaDTO;
import com.rfsystems.estudos.spring.batch.model.EmpresaEntity;
import com.rfsystems.estudos.spring.batch.processor.DocProcessor;
import com.rfsystems.estudos.spring.batch.processor.FileDeletingTasklet;
import com.rfsystems.estudos.spring.batch.utils.FileReader;
import com.rfsystems.estudos.spring.batch.writer.EmpresaDBWriter;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.sql.DataSource;
import java.io.File;
import java.util.List;

@Configuration
public class Demo1 {

    private JobBuilderFactory jobBuilderFactory;
    private StepBuilderFactory stepBuilderFactory;
    private DocProcessor docProcessor;
    private DataSource dataSource;
    private EmpresaDBWriter empresaDBWriter;

    @Value( "${files.path}")
    private String path;

    @Autowired
    public Demo1(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, DocProcessor docProcessor, DataSource dataSource, EmpresaDBWriter empresaDBWriter){
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.docProcessor = docProcessor;
        this.dataSource = dataSource;
        this.empresaDBWriter = empresaDBWriter;
    }

    //criando o job
    @Qualifier(value = "demo1")
    @Bean
    public org.springframework.batch.core.Job demo1Job() throws Exception {
        return this.jobBuilderFactory.get("demo1")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .next(step2())
                .build();
    }

    //definindo o 1 step e passando o leitor, o processador e o escritor
    //adicionei um taskexecutor para melhorar a performance de execução com o multi-thread
    @Bean
    public Step step1() throws Exception {
        return this.stepBuilderFactory.get("step1")
                .<EmpresaDTO, EmpresaEntity>chunk(10)
                .reader(multiResourceItemReader())
                .processor(docProcessor)
                .writer(empresaDBWriter)
                .taskExecutor(taskExecutor())
                .build();
    }

    @Bean
    public Step step2() {
        FileDeletingTasklet task = new FileDeletingTasklet();
        task.setResources(FileReader.reader(path));
        return stepBuilderFactory.get("step2")
                .tasklet(task)
                .build();
    }

    @Bean
    @StepScope
    Resource inputFileResource(@Value("#{jobParameters[fileName]}") final String fileName) throws Exception {
        return new ClassPathResource(fileName);
    }
    //aqui adicionei um multi resource reader para aumentar a performance de leitura de multiplos arquivos
    @Bean
    @StepScope
    public MultiResourceItemReader<EmpresaDTO> multiResourceItemReader()  {
        MultiResourceItemReader<EmpresaDTO> resourceItemReader = new MultiResourceItemReader<EmpresaDTO>();
        resourceItemReader.setResources(FileReader.reader(path));
        resourceItemReader.setDelegate(empresaReader());
        return resourceItemReader;
    }

    @Bean
    @StepScope
    public MultiResourceItemReader<EmpresaDTO> multiResourceDeleteItemReader()  {
        MultiResourceItemReader<EmpresaDTO> resourceItemReader = new MultiResourceItemReader<EmpresaDTO>();
        FileReader.remove(path);
        return resourceItemReader;
    }

    //definindo os headers e delimitador das linhas do txt
    @Bean
    public FlatFileItemReader<EmpresaDTO> empresaReader(){
        FlatFileItemReader<EmpresaDTO> reader = new FlatFileItemReader<>();
        reader.setLineMapper(new DefaultLineMapper<EmpresaDTO>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames("codigo", "chave");
                setDelimiter(";");
            }});
            setFieldSetMapper(new EmpresaFileRowMapper());
        }});
        return reader;
    }

    //definindo 4 theads para essa execução, aqui poderíamos ter outras abordagens, como processamento paralelo, porém,
    //seria necessário mais alguns testes de massa para definir a melhor opção
    @Bean
    public TaskExecutor taskExecutor(){
        SimpleAsyncTaskExecutor simpleAsyncTaskExecutor = new SimpleAsyncTaskExecutor();
        simpleAsyncTaskExecutor.setConcurrencyLimit(4);
        return simpleAsyncTaskExecutor;
    }

}
